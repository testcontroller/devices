# Devices descirptions for TestController software

For more details check:
* **EEVBlog Forum thread**: https://www.eevblog.com/forum/testgear/program-that-can-log-from-many-multimeters/
* **TestController homepage**: https://lygte-info.dk/project/TestControllerIntro%20UK.html
* **TestController -> Configuration of a SCPI devices**: https://lygte-info.dk/project/TestControllerConfigDevice%20UK.html

