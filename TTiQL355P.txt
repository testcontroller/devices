
#metadef

#metadef
#idString THURLBY-THANDAR, QL355P, 
#name TTI QL355P

#meta
#idString THURLBY THANDAR, QL355P, 
#name TTi QL355P
#handle QL355
#port 9221 GPIB

; A list of possible column name with unit and formatter (SI, Time, Int, D0..D6)
#value Voltage V D3 
#value Current A D3 
#value VoltageSet V D3 
#value CurrentSet A D3 

; How to poll for data, this is used for table and #values?
; a #askMode, #cmdMode and #prepareSample is used before this is string is used.
; This is a single line command
#askValues V1O?;I1O?;V1?;I1?

; Format of answer: f=float, u=remove trailing letters, x=skip
#askValuesReadFormat uuxfxf

; Accept this delay when reading values (seconds)
;#readingDelay 2

; Mode change have a longer delay on reading values (seconds)
;#modeChangeDelay 10

; Switch meter to this mode during start, leave empty to avoid any switching
#initialMode 

; String to ask about actual meter mode, 
; This is a single line command
#askMode 

; When one of these commands are used through the command interface a new configuration will be done before using #askMode
; Only one word for each #mayModifyMode
; Specify command without initial colon and in the shortest possible form
;#mayModifyMode 



; Prepare the meter to response to #askValues
#prepareSample


; Initial commands to meter when establishing connection
#initCmd  


; Final command to meter before breaking connection
#finalCmd OP1 0;V1 0;I1 0;local

#outputOff OP1 0


#interfaceType PS
#interface setVoltage V1 (value)
#interface setCurrent I1 (value)
#interface setOVP OVP1 (value)
#interface setOCP OCP1 (value)
#interface setOn OP1 (value)
#interface getVoltage V1?
:readFormat: xf
#interface getCurrent I1?
:readFormat: xf
#interface getOVP OVP1?
:readFormat: xf
#interface getOCP OCP1?
:readFormat: xf
#interface getOn OP1?
#interface readVoltage 0
#interface readCurrent 1



; Strings to configure device in different modes
; First parameter must match a #value (4 parameter) and second parameter must match what #askMode returns
; First parameter is also used in shortcut menu
;#cmdMode 

; Setup menu functions
; The parameters on the first line is: type name page 
; Settings on the following lines are:
; read: SCPI to read value from device, is used to synchronize when opening setup page or setting equal named fields. Not always used
; readformat: Parse read value.
; write: Send the value to device, this field is used in combination with data fields in the details to send updates to device.
; tip: Add this tip to all components for this input field.
#cmdSetup radio Voltage
:read: V1?;
:readFormat: xf
:write: V1 #; OP1 1
:tip: Setup this voltage, turn output on
3.3V 3.3
5V 5.0
12V 12.0
24V 24.0

#cmdSetup radio Current
:read: I1?
:readFormat: xf
:write: I1
:tip: Output will current limit at this current
100mA 0.1
300mA 0.3
1A 1.0
3A 3.0
5A 5.0

#cmdSetup radio Range
:read: RANGE1?
:readFormat: xf
:write: RANGE1
:update: Output
15V_5A 0
35V_3A 1
15V_0.5A 2


#cmdSetup buttonsOn Output
:read: OP1?
:write: TRIPRST;*cls;OP1
:tip: Turn output on or off
:updatealloff:
Off 0
On 1

#cmdSetup buttons RemoteSense
:write: SENSE1
:tip: Use sense wires to get exact voltage at target
Off 0
On 1


#cmdSetup number Voltage
:read: V1?
:readformat: xf
:write: V1
:tip: Setup this voltage, do not turn output on/off
Volt 0 35


#cmdSetup number Current
:read: I1?
:readformat: xf
:write: I1
:tip: Setup this current, do not turn output on/off
Amps 0.0001 5


#cmdSetup number OverVoltage 
:read: OVP1?
:readformat: xf
:write: OVP1
:tip: Output will turn off if it is above this voltage
Volt 1 40

#cmdSetup number OverCurrent 
:read: OCP1?
:readformat: xf
:write: OCP1
:tip: Output will turn off if it is above this current
Amps 0.01 5.5



