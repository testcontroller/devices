
#idString HKJ,VirtualPatternGenerator,
#name VirtualPatternGenerator
#handle VPG
#port none

#helpurl http://lygte-info.dk/project/TestControllerVirtualDevices%20UK.html#Virtual_pattern_generator_(VPG)

#outputOff on 0

#interfaceType VPG

; Strings to configure device in different modes
; First parameter must match a #value (4 parameter) and second parameter must match what #askMode returns
; First parameter is also used in shortcut menu
;#cmdMode 

; Setup menu functions
; The parameters on the first line is: type name page 
; Settings on the following lines are:
; read: SCPI to read value from device, is used to synchronize when opening setup page or setting equal named fields. Not always used
; askValuesReadFormat: Parse read value.
; write: Send the value to device, this field is used in combination with data fields in the details to send updates to device.
; tip: Add this tip to all components for this input field.


#cmdSetup checkBox Repeat
:read: repeat?
:write: repeat
:tip: Run pattern once or repeat it
On 0 1

#cmdSetup checkBox Repeat
:read: stayAtFinalValue?
:write: stayAtFinalValue
:tip: When not repeating the output level will stay at the final value
Stay 0 1


#cmdSetup multi Entry
:read: entry? 0
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 1
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 2
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 3
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 4
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 5
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 6
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 7
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 8
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1

#cmdSetup multi Entry
:read: entry? 9
:write: entry
:tip: Entry for pattern with time and value and smooth/abrupt change to value
info #
number Time _ 0 1000
number Value _ -1G 1G
checkbox Ramp 0 1



#cmdSetup buttonsOn Output
:read: on?
:write: on
:updatealloff:
Off 0
On 1



