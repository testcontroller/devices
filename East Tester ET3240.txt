
; TestController must be restarted before any changes in this file will be used.

; Manual is here: https://lygte-info.dk/project/TestControllerConfigDevice%20UK.html

#idString ZC,ET3240,
#name East Tester ET3240
#handle ET3240
#port comnobaud
; Alternate port types: comfixedbaud, comnobaud or com, com lets the user define baudrate on the "Load devices" page
#baudrate 115200


#notes The meter do not support temperature on the SCPI interface.
Sometimes the meter will end up showing 200A range, press SHIFT Exit and arrow down/up keys on meter to fix it.

; A list of possible column name with unit and formatter (SI, Time, Int, D0..D6) 
; Format: #value ColumnName Unit Format {Selector}
; Selector is only used when column layout varies with mode, this often require the use of #cmdMode
#value VoltageDC V D4 DC_Voltage,Diode
#value CurrentDC A si DC_Current
#value VoltageAC V D4 AC_Voltage
#value CurrentAC A si AC_Current
#value Resistance ohm si 2W_Resistance,4W_Resistance,Continuity
#value Capacitance F si Capacitance
#value Frequency Hz si Frequency
#value Period s si Period
#value DutyCycle - d1 Duty_Cycle

#eol \ |\n

; How to poll for data, this is used for table and #values?
; a #askMode, #cmdMode and #prepareSample is used before this is string is used.
; Number of returned values must match number of columns defined with #value
; This is a single line command
#askValues read?

; Format of answer: f=float, u=remove trailing letters, x=skip, *=Zero upper case values if this value is 0
;#askValuesReadFormat 

; Accept this delay when reading values (seconds)
#readingDelay 2

; Mode change have a longer delay on reading values (seconds)
#modeChangeDelay 10


#interfaceType DMM BMM
#interface readValue 0

; String to ask about actual meter mode, it is mostly used for DMM's
; This is a single line command
#askMode CONF?
#askModeMathFormat getElement(value,0); 

; When one of these commands are used through the command interface a new configuration will be done before using #askMode
; Only one word for each #mayModifyMode
; Specify command without initial colon and in the shortest possible form
;#mayModifyMode 


; Prepare the meter to response to #askValues
;#prepareSample arm:sour imm;:arm:count 1;:trig:sour imm;:trig:count 1;:trig:samp:count 1;init

; Initial commands to meter when establishing connection, used to disable local control
#initCmd trig auto

; Final command to meter before breaking connection, used to restore local control
#finalCmd syst:local

; Used to turn output off for power supplies, generators and electronic loads
;#outputOff 

#cmdModeLayout 3 4


; Strings to configure device in different modes, using these forces a mode check/change
; When used to select mode first parameter must match a #value (4 parameter) and second parameter must match what #askMode returns
; It can also be used for just checking the mode, then the #value (4 parameter) must match what #askMode returns
; First parameter is also used in shortcut menu
#cmdMode DC_Voltage DCV
conf:volt:dc null;[200]

#cmdMode AC_Voltage ACV
conf:volt:ac null;[200]

#cmdMode 2W_Resistance R2
conf:res null;[200]

#cmdMode DC_Current DCI
conf:curr:dc null;[200]

#cmdMode AC_Current ACI
conf:curr:ac null;[200]

#cmdMode 4W_Resistance R4
conf:fres null;[200]

#cmdMode Diode DIOD
conf:diod;[200]

#cmdMode Continuity CONT
conf:cont;[200]

#cmdMode Capacitance CAP
conf:cap null;[200]

#cmdMode Frequency FREQ
conf:freq;[200]

#cmdMode Period PER
conf:per;[200]

#cmdMode Duty_Cycle DUTY
conf:duty;[200]


;DC_VOLTAGE ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range DC_Voltage
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range DC_Voltage
:write: func:rang:value (getElement("200mV 2V 20V 200V 1000V",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F");
200mV 0
2V 1
20V 2
200V 3
1000V 4
:updatemodechange:

#cmdSetup comboboxHot Aperture DC_Voltage
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:


;AC_VOLTAGE ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range AC_Voltage
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range AC_Voltage
:write: func:rang:value (getElement("200mV 2V 20V 200V 750V",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F");
200mV 0
2V 1
20V 2
200V 3
750V 4
:updatemodechange:

#cmdSetup comboboxHot Aperture AC_Voltage
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:

;DC_CURRENT  ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range DC_Current
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range DC_Current
:write: func:rang:value (getElement("200uA 2mA 20mA 200mA 2A 10A",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F G");
200uA 0
2mA 1
20mA 2
200mA 3
2A 4
12A 5
:updatemodechange:

#cmdSetup comboboxHot Aperture DC_Current
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:

;AC_CURRENT  ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range AC_Current
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range AC_Current
:write: func:rang:value (getElement("200uA 2mA 20mA 200mA 2A 10A",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F G");
200uA 0
2mA 1
20mA 2
200mA 3
2A 4
12A 5
:updatemodechange:

#cmdSetup comboboxHot Aperture AC_Current
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:

;Ohm 2 wire  ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range 2W_Resistance
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range 2W_Resistance
:write: func:rang:value (getElement("200r 2kr 20kr 200kr 2Mr 10Mr 200Mr",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F G H");
200ohm 0
2kOhm 1
20kOhm 2
200kOhm 3
2Mohm 4
20Mohm 5
200Mohm 6
:updatemodechange:

#cmdSetup comboboxHot Aperture 2W_Resistance
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:

;Ohm 4 wire  ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range 4W_Resistance
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range 4W_Resistance
:write: func:rang:value (getElement("200r 2kr 20kr 200kr 2Mr 10Mr 200Mr",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F G H");
200ohm 0
2kOhm 1
20kOhm 2
200kOhm 3
2Mohm 4
20Mohm 5
200Mohm 6
:updatemodechange:

#cmdSetup comboboxHot Aperture 4W_Resistance
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:

;Capacitance  ---------------------------------------------------------------------------------------------------------

#cmdSetup radio Range Capacitance
:read: func:rang:auto?
:string:
:write: func:rang:auto #;[100]
AUTO ON
Manual OFF
:updatemodechange:

#cmdSetup comboboxHot Range Capacitance
:write: func:rang:value (getElement("2nf 20nf 200nf 2uf 20uf 200if 2mf",value));[200]
:read: func:rang:value?;[100]
:readmath: listIndex(value,"B C D E F G H");
2nF 0
20nF 1
200nF 2
2uF 3
20uF 4
200uF 5
2mF 6
:updatemodechange:

#cmdSetup comboboxHot Aperture Capacitance
:write: APER #;[200]
:read: APER?;[100]
Slow SLOW
Medium MIDDLE
Fast FAST
:updatemodechange:


;---------------------------------------------------------------------------------------------------------

#cmdSetup selector Mode_settings
:read: CONF?
:readmath: getElement(value,0); 
:updatemodechange:
DCV DC_Voltage.
ACV AC_Voltage.
DCI DC_Current.
ACI AC_Current.
DIOD Diode.
FREQ Frequency.
R2 2W_Resistance.
R4 4W_Resistance.
CAP Capacitance.
CONT Continuity.
PER Period.
DUTY Duty_cycle


#cmdSetup slider Display_brightness
:write: SYST:LIGHT #;[100]
:read: SYST:LIGHT?
:readformat: xi
_ 1 5