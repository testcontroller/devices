
#metadef
#idString EZA,EZA EZ-735,
#name EZA EZ-735
#handle EZ735
#subDriver 5
#baudrate 2400N81Dr


#metadef
#idString Victor,Victor 70C,
#name Victor 70C
#handle V70C
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString Victor,Victor 86C,
#name Victor 86C
#handle V86C
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString Voltcraft,Voltcraft VC850,
#name Voltcraft VC850
#handle VC850
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString SparkFun,SparkFun 70C,
#name SparkFun 70C
#handle S70C
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString Peaktech,Peaktech 2025,
#name Peaktech 2025
#handle P2025
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString ProsKit,ProsKit MT-1820,
#name ProsKit MT-1820
#handle MT1820
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString UNI-T,UNI-T UT60A,
#name UNI-T UT60A
#handle UT60
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString UNI-T,UNI-T UT60D,
#name UNI-T UT60D
#handle UT60
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString DigiTek,DigiTek DT-9602R+,
#name DigiTek DT-9602R+
#handle DT9602R
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString UNI-T,UT61B,
#name UNI-T UT61B
#handle UT61
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString UNI-T,UT61C,
#name UNI-T UT61C
#handle UT61
#subDriver 5
#baudrate 2400N81Dr

#metadef
#idString UNI-T,UT61D,
#name UNI-T UT61D
#handle UT61
#subDriver 5
#baudrate 2400N81Dr


#metadef
#idString UNI-T,UT61E,
#name UNI-T UT61E
#handle UT61
#subDriver 1
#baudrate 19200O71Dr

#metadef
#idString UNI-T,UT71E,
#name UNI-T UT71E
#handle UT71E
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString UNI-T,UT71A,
#name UNI-T UT71A
#handle UT71A
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString UNI-T,UT71B,
#name UNI-T UT71B
#handle UT71B
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString UNI-T,UT71C,
#name UNI-T UT71C
#handle UT71C
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString UNI-T,UT71D,
#name UNI-T UT71D
#handle UT71D
#subDriver 2
#baudrate 2400O71Dr


#metadef
#idString Voltcraft,VC-920,
#name Voltcraft VC-920
#handle VC920
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString Voltcraft,VC-960,
#name Voltcraft VC-960
#handle VC960
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString Voltcraft,VC-940
#name Voltcraft VC-940 COM
#handle VC940
#subDriver 4
#baudrate 2400O71Dr

#metadef
#idString Tenma,72-7730
#name Tenma 72-7730
#handle T730
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString Tenma,72-7732
#name Tenma 72-7732
#handle T732
#subDriver 2
#baudrate 2400O71Dr

#metadef
#idString Tenma,72-9380A
#name Tenma 72-9380A
#handle T9380A
#subDriver 2
#baudrate 2400O71Dr



#metadef
#idString Mastech,MS8040,
#name Mastech MS8040
#handle M8040
#subDriver 3
#baudrate 19200O71Dr


#metadef
#idString Mastech,MS8218,
#name Mastech MS8218
#handle M8218
#subDriver MastechMS8218
#driver DMM2
#baudrate 2400N81Dr




#meta

#idString UNI-T,UT61E,
#name UNI-T UT61E
#handle UT61
#driver UNITDMM
#subDriver 1
#port comfixedbaud
#baudrate 19200O71Dr
#help SimpleDMM


